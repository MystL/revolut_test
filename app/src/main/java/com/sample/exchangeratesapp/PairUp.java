package com.sample.exchangeratesapp;

import rx.functions.Func2;

/**
 * Created by melvin on 5/11/17.
 */

public class PairUp<A, B> implements Func2<A, B, Tuple2<A, B>> {
    @Override
    public Tuple2<A, B> call(A a, B b) {
        return new Tuple2<>(a, b);
    }
}
