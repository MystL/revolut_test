package com.sample.exchangeratesapp;

import rx.functions.Func1;

/**
 * Created by melvin on 5/11/17.
 */

public class NonNullFilter implements Func1<Object, Boolean> {
    @Override
    public Boolean call(Object o) {
        return o != null;
    }
}
