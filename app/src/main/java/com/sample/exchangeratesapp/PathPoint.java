package com.sample.exchangeratesapp;

/**
 * Created by melvin on 24/10/17.
 */

public class PathPoint {

    float x, y;

    public PathPoint(float _x, float _y){
        x = _x;
        y = _y;
    }

}
