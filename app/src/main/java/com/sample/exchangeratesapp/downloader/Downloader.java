package com.sample.exchangeratesapp.downloader;

import java.net.URL;

import rx.Observable;

/**
 * Created by melvin on 24/10/17.
 */

public class Downloader{

    private DownloadHelperInterface helperInterface;

    public Downloader(DownloadHelperInterface helperInterface){
        this.helperInterface = helperInterface;
    }

    public void download(URL fileURL){
        helperInterface.download(fileURL);
    }

    public Observable<String> getDownloadCompletedObservable(){
        return helperInterface.getDownloadCompletedObs();
    }

    public Observable<Exception> getOnDownloadErrorObservable(){
        return helperInterface.getOnDownloadErrorObs();
    }
}
