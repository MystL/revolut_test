package com.sample.exchangeratesapp.downloader;

import android.content.Context;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import okio.BufferedSink;
import okio.Okio;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

/**
 * Created by melvin on 24/10/17.
 */

public class FileDownloader implements DownloadHelperInterface{

    private BehaviorSubject<String> downloadCompletedSubject;
    private PublishSubject<Exception> onDownloadErrorSubject;
    private OkHttpClient client;
    private Context context;


    public FileDownloader(Context context){
        this.context = context;
        client = new OkHttpClient();
        downloadCompletedSubject = BehaviorSubject.create("");
        onDownloadErrorSubject = PublishSubject.create();
    }


    @Override
    public void download(URL fileURL) {
        Request request = new Request.Builder().url(fileURL).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                onDownloadErrorSubject.onNext(e);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if(response.code() == 200){
                    File downloadedFile = new File(context.getCacheDir(), "rates.sqlite");
                    BufferedSink sink = Okio.buffer(Okio.sink(downloadedFile));
                    sink.writeAll(response.body().source());
                    sink.close();
                    downloadCompletedSubject.onNext(downloadedFile.getAbsolutePath());
                } else {
                    onDownloadErrorSubject.onNext(new Exception(response.body().toString()));
                }
                response.body().close();
            }
        });
    }

    @Override
    public Observable<String> getDownloadCompletedObs() {
        return downloadCompletedSubject.observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Exception> getOnDownloadErrorObs() {
        return onDownloadErrorSubject;
    }


}
