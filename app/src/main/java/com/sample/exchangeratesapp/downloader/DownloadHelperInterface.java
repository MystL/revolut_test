package com.sample.exchangeratesapp.downloader;

import java.net.URL;

import rx.Observable;

/**
 * Created by melvin on 24/10/17.
 */

public interface DownloadHelperInterface {

    void download(URL fileURL);

    Observable<String> getDownloadCompletedObs();

    Observable<Exception> getOnDownloadErrorObs();
}
