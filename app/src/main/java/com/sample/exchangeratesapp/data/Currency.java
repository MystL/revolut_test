package com.sample.exchangeratesapp.data;

/**
 * Created by melvin on 24/10/17.
 */

public class Currency {

    private String code;

    public Currency(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Currency currency = (Currency) o;

        return code != null ? code.equals(currency.code) : currency.code == null;

    }

    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }
}
