package com.sample.exchangeratesapp.data;

/**
 * Created by melvin on 24/10/17.
 */

public class ExchangeRates {

    private Currency currency;
    private double rates;
    private int time;

    public ExchangeRates(Currency currency, double rates, int time){
        this.currency = currency;
        this.rates = rates;
        this.time = time;
    }

    public Currency getCurrency() {
        return currency;
    }

    public double getRates() {
        return rates;
    }

    public int getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExchangeRates that = (ExchangeRates) o;

        if (Double.compare(that.rates, rates) != 0) return false;
        if (time != that.time) return false;
        return currency != null ? currency.equals(that.currency) : that.currency == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = currency != null ? currency.hashCode() : 0;
        temp = Double.doubleToLongBits(rates);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + time;
        return result;
    }
}
