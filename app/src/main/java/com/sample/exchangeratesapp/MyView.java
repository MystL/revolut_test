package com.sample.exchangeratesapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.SurfaceView;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;

public class MyView extends View {

    private Paint paint;
    private Path path;

    public MyView(Context context) {
        super(context);
        init();

    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE);

        path = new Path();
        path.moveTo(100, 100);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = 3000;
        int height = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // flip the canvas to normal coord
        canvas.translate(0, canvas.getHeight());
        canvas.scale(1, -1);
        super.onDraw(canvas);

        canvas.drawPath(path, paint);
    }

    public void updatePath(PathPoint pathPoint) {
        path.lineTo(pathPoint.x, pathPoint.y);
        postInvalidateDelayed(100);
    }


}

