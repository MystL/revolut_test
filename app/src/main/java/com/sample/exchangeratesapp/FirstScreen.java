package com.sample.exchangeratesapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sample.exchangeratesapp.database.DatabaseHelper;
import com.sample.exchangeratesapp.downloader.Downloader;
import com.sample.exchangeratesapp.downloader.FileDownloader;

import java.io.File;
import java.net.URL;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by melvin on 24/10/17.
 */

public class FirstScreen extends AppCompatActivityWithSubscriptions {

    private Downloader downloader;
    private ProgressBar progressBar;
    private TextView textDisplay;
    private Button btn_import;
    private Button btn_useExisting;
    private Observable<View> importButtonClickObs;
    private Observable<View> useExistingButtonClickObs;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_screen_layout);

        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        textDisplay = (TextView) findViewById(R.id.textdisplay);
        btn_import = (Button) findViewById(R.id.btn_import);
        importButtonClickObs = ObservableOps.clicks(btn_import).observeOn(AndroidSchedulers.mainThread());
        btn_useExisting = (Button) findViewById(R.id.btn_use_existing);
        useExistingButtonClickObs = ObservableOps.clicks(btn_useExisting).observeOn(AndroidSchedulers.mainThread());

        FileDownloader okHttpDownloader = new FileDownloader(this);
        downloader = new Downloader(okHttpDownloader);
        databaseHelper = new DatabaseHelper(getApplicationContext());

        if(hasExistingDBFile()){
            showExistingDBAvailable();
        } else {
            showInstructionsToImportDB();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        addSubscriptions(importButtonClickObs.subscribe(new MySubscriber<View>() {
            @Override
            public void onNext(View view) {
                downloadDB();
            }
        }));

        addSubscriptions(useExistingButtonClickObs.subscribe(new MySubscriber<View>() {
            @Override
            public void onNext(View view) {
                //TODO
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

            }
        }));

        addSubscriptions(downloader.getDownloadCompletedObservable().distinctUntilChanged().subscribe(new MySubscriber<String>() {
            @Override
            public void onNext(String filePath) {
                if(!filePath.isEmpty()){
                    try{
                        File cachedDBFile = new File(filePath);
                        databaseHelper.copyFileToDatabase(cachedDBFile);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }));

        addSubscriptions(databaseHelper.getDatabaseReadyObservable().subscribe(new MySubscriber<Boolean>() {
            @Override
            public void onNext(Boolean bReady) {
                if(bReady){
                    showDownloadSuccessMessage();
                }
            }
        }));

        addSubscriptions(downloader.getOnDownloadErrorObservable().observeOn(AndroidSchedulers.mainThread()).subscribe(new MySubscriber<Exception>() {
            @Override
            public void onNext(Exception e) {
                showDownloadError();
                e.printStackTrace();
            }
        }));
    }

    private void downloadDB(){
        try{
            URL dbFileUrl = new URL("https://bitbucket.org/ezubkov/android-test-task/downloads/rates.sqlite");
            downloader.download(dbFileUrl);
            showDownloadingFileMessage();
        }catch (Exception e){
            showDownloadError();
        }
    }

    private boolean hasExistingDBFile(){
        try{
            File file = new File(getCacheDir(), "rates.sqlite");
            return file.exists();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }

    private void showExistingDBAvailable(){
        progressBar.setVisibility(View.GONE);
        textDisplay.setText(getString(R.string.has_existing_db_file_msg));
    }

    private void showInstructionsToImportDB(){
        btn_useExisting.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        textDisplay.setText(getString(R.string.instruct_to_import));
    }

    private void showDownloadingFileMessage(){
        btn_useExisting.setVisibility(View.GONE);
        btn_import.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        textDisplay.setText(getString(R.string.downloading_db_msg));
    }

    private void showDownloadSuccessMessage(){
        progressBar.setVisibility(View.GONE);
        textDisplay.setText(getString(R.string.download_success));
        btn_useExisting.setText("Show Rates");
        btn_useExisting.setVisibility(View.VISIBLE);
    }

    private void showDownloadError(){
        progressBar.setVisibility(View.GONE);
        textDisplay.setText(getString(R.string.error_downloading_db_msg));
    }
}
