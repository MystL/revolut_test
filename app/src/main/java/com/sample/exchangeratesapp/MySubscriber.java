package com.sample.exchangeratesapp;

import android.util.Log;

import rx.Subscriber;

/**
 * Created by melvin on 24/10/17.
 */

public abstract class MySubscriber<T> extends Subscriber<T>{

    @Override
    public void onCompleted() {
        Log.i(this.getClass().getCanonicalName(), "completed");

    }

    @Override
    public void onError(Throwable e) {
        if (e != null) {
            Log.e(this.getClass().getCanonicalName(), "error in subscriber : " + e.getMessage());
            e.printStackTrace();
        }
    }

}
