package com.sample.exchangeratesapp;

import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

/**
 * Created by melvin on 25/10/17.
 */

public class AppCompatActivityWithSubscriptions extends AppCompatActivity {

    private List<Subscription> subscriptions = new ArrayList<>();

    @Override
    protected synchronized void onPause() {
        super.onPause();
        for (Subscription s : subscriptions) {
            if (!s.isUnsubscribed()) {
                s.unsubscribe();
            }
        }
        subscriptions.clear();
    }

    public synchronized void addSubscriptions(Subscription s) {
        subscriptions.add(s);
    }

}
