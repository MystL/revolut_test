package com.sample.exchangeratesapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sample.exchangeratesapp.graph.CurrencyGraphDrawHelper;
import com.sample.exchangeratesapp.graph.GraphHelper;

public class MainActivity extends AppCompatActivity {

    private GraphHelper graphHelper;
    private MyView lineView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lineView = (MyView) findViewById(R.id.lineview1);

        graphHelper = new GraphHelper(new CurrencyGraphDrawHelper(getApplicationContext()));


    }

    @Override
    protected void onResume() {
        super.onResume();

//        graphHelper.getInitialGraphPointsObs().subscribe(new MySubscriber<Map<String, List<PathPoint>>>() {
//            @Override
//            public void onNext(Map<String, List<PathPoint>> stringListMap) {
//
//            }
//        });
//
//        graphHelper.getOnGraphPointsUpdatedObs().subscribe(new MySubscriber<Map<String, PathPoint>>() {
//            @Override
//            public void onNext(Map<String, PathPoint> newPathPointMap) {
//
//            }
//        });
    }
}
