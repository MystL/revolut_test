package com.sample.exchangeratesapp.graph;

import com.sample.exchangeratesapp.PathPoint;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * Created by melvin on 5/11/17.
 */

public class GraphHelper{

    private GraphHelperInterface helperInterface;

    public GraphHelper(GraphHelperInterface helperInterface){
        this.helperInterface = helperInterface;
    }

    public Observable<Map<String, List<PathPoint>>> getInitialGraphPointsObs(){
        return helperInterface.getPreloadDataPointsObs();
    }

    public Observable<Map<String, PathPoint>> getOnGraphPointsUpdatedObs(){
        return helperInterface.getOnDataPointLoadedObs();
    }



}
