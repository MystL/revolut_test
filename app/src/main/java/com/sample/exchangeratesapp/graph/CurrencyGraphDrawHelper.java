package com.sample.exchangeratesapp.graph;

import android.content.Context;

import com.sample.exchangeratesapp.MySubscriber;
import com.sample.exchangeratesapp.PathPoint;
import com.sample.exchangeratesapp.R;
import com.sample.exchangeratesapp.data.Currency;
import com.sample.exchangeratesapp.data.ExchangeRates;
import com.sample.exchangeratesapp.database.CurrenciesDataHelper;
import com.sample.exchangeratesapp.database.RatesDataHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Func2;
import rx.subjects.BehaviorSubject;

/**
 * Created by melvin on 5/11/17.
 */

public class CurrencyGraphDrawHelper implements GraphHelperInterface{

    private Context context;
    private CurrenciesDataHelper currenciesDataHelper;
    private RatesDataHelper ratesDataHelper;
    private BehaviorSubject<HashSet<Currency>> currenciesListSubject;

    public CurrencyGraphDrawHelper(Context context){
        this.context = context;
        currenciesListSubject = BehaviorSubject.create();
        String authority = context.getString(R.string.my_content_provider_authority);
        currenciesDataHelper = new CurrenciesDataHelper(authority, context.getContentResolver());
        ratesDataHelper = new RatesDataHelper(authority, context.getContentResolver());
        subscribeToAvailableCurrencyObs();
        getRatesBeforeTime(10);
    }

    private void subscribeToAvailableCurrencyObs(){
        currenciesDataHelper.getCurrenciesObservable().subscribe(new MySubscriber<List<String>>() {
            @Override
            public void onNext(List<String> currencies) {
                HashSet<Currency> currencyHashSet = new HashSet<>();
                for(String s : currencies){
                    currencyHashSet.add(new Currency(s));
                }
                currenciesListSubject.onNext(currencyHashSet);
            }
        });
    }

    private Observable<List<ExchangeRates>> getRatesForTime(final int sec){
        return Observable.combineLatest(currenciesListSubject, ratesDataHelper.getRatesAtTime(sec),
                new Func2<HashSet<Currency>, Map<String, Double>, List<ExchangeRates>>() {
            @Override
            public List<ExchangeRates> call(HashSet<Currency> currencies, Map<String, Double> ratesMap) {
                List<ExchangeRates> ratesList = new ArrayList<>();
                for(Map.Entry<String, Double> ev : ratesMap.entrySet()){
                    for(Currency currency : currencies){
                        if(currency.getCode().equals(ev.getKey())){
                            ratesList.add(new ExchangeRates(currency, ev.getValue(), sec));
                        }
                    }
                }
                return ratesList;
            }
        });
    }

    private void getRatesBeforeTime(int sec){
        ratesDataHelper.getRatesBeforeTime(sec);
    }

    @Override
    public Observable<Map<String, List<PathPoint>>> getPreloadDataPointsObs() {
        return null;
    }

    @Override
    public Observable<Map<String, PathPoint>> getOnDataPointLoadedObs() {
        return null;
    }



}
