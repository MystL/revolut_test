package com.sample.exchangeratesapp.graph;

import com.sample.exchangeratesapp.PathPoint;

import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * Created by melvin on 5/11/17.
 */

public interface GraphHelperInterface {

    Observable<Map<String, List<PathPoint>>> getPreloadDataPointsObs();

    Observable<Map<String, PathPoint>> getOnDataPointLoadedObs();

}
