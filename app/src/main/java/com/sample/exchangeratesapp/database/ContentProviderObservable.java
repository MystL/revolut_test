package com.sample.exchangeratesapp.database;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.sample.exchangeratesapp.NonNullFilter;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.subjects.BehaviorSubject;

/**
 * Created by melvin on 5/11/17.
 */

public abstract class ContentProviderObservable<T> {


    public abstract Uri getUri();


}
