package com.sample.exchangeratesapp.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by melvin on 25/10/17.
 */

public class MyContentProvider extends ContentProvider {

    public static final String PROTOCOL = "content://";
    public static final String CURRENCIES_PATH = "/" + SQL.CURRENCIES_TABLE_NAME;
    public static final String RATES_PATH = "/" + SQL.RATES_TABLE_NAME;
    public static final int DATABASE_VERSION = 1;

    private DatabaseHelper helper;

    public static Uri getCurrenciesUri(String authority){
        return Uri.parse(PROTOCOL + authority + CURRENCIES_PATH);
    }

    public static Uri getRatesUri(String authority){
        return Uri.parse(PROTOCOL + authority + RATES_PATH);
    }

    @Override
    public boolean onCreate() {
        helper = new DatabaseHelper(getContext(), DATABASE_VERSION);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = helper.getReadableDatabase();
        String tableName = getTableName(uri);
        return makeQuery(uri, db, tableName, projection, selection, selectionArgs, sortOrder);
    }

    private Cursor makeQuery(Uri uri, SQLiteDatabase db, String tableName, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = db.query(tableName, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    private String getTableName(Uri uri){
        String path = uri.getPath();
        switch (path){
            case CURRENCIES_PATH:
                return SQL.CURRENCIES_TABLE_NAME;
            case RATES_PATH:
                return SQL.RATES_TABLE_NAME;
            default:
                throw new IllegalArgumentException("Unsupported URI : " + uri);
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        // no need for now
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        // no need for now
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        // no need for now
        return 0;
    }
}
