package com.sample.exchangeratesapp.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sample.exchangeratesapp.BuildConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by melvin on 24/10/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private final String localDBPath;
    private PublishSubject<Boolean> onDatabaseReadySubject;
    private static final String LOCAL_DB_NAME = "rates.sqlite";

    public DatabaseHelper(Context context, int version) {
        super(context, LOCAL_DB_NAME, null, version);
        onDatabaseReadySubject = PublishSubject.create();
        localDBPath = "/data/data/" + BuildConfig.APPLICATION_ID + "/databases/";
    }

    public DatabaseHelper(Context context){
        this(context, MyContentProvider.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //...
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        dropTables(db);
        onCreate(db);
    }

    private void dropTables(SQLiteDatabase db) {
        Log.w("XXX", "dropping tables");
        for (String table : SQL.ALL_TABLES) {
            db.execSQL("DROP TABLE IF EXISTS " + table);
        }
    }

    public Observable<Boolean> getDatabaseReadyObservable(){
        return onDatabaseReadySubject;
    }

    public void copyFileToDatabase(File file){
        if(file == null || !file.exists()){
            throw new IllegalArgumentException("Cannot Copy Unknown File");
        }
        createLocalDatabase();
        copyDatabase(file);
    }

    private void createLocalDatabase(){
        if(!checkFileAvailability(localDBPath + LOCAL_DB_NAME)){
            this.getReadableDatabase();
        } else {
            Log.w("XXX", "Already has DB, not creating");
        }
    }

    private boolean checkFileAvailability(String filePath) {
        boolean fileExists = false;
        try {
            File dbfile = new File(filePath);
            fileExists = dbfile.exists();
        } catch (Exception e) {
            Log.i("XXX", "Database doesn't exist");
        }
        return fileExists;
    }

    private void copyDatabase(File cachedDBFile) {
        try {
            int length;
            byte[] buffer = new byte[4 * 1024];
            FileInputStream fis = new FileInputStream(cachedDBFile);
            FileOutputStream fos = new FileOutputStream(localDBPath + LOCAL_DB_NAME);
            while ((length = fis.read(buffer)) > 0) {
                fos.write(buffer, 0, length);
            }
            fos.close();
            fos.flush();
            fis.close();
            onDatabaseReadySubject.onNext(true);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

}
