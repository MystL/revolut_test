package com.sample.exchangeratesapp.database;

import android.content.ContentResolver;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import rx.Observable;
import rx.subjects.BehaviorSubject;

/**
 * Created by melvin on 5/11/17.
 */

public class RatesDataHelper {

    private String authority;
    private ContentResolver contentResolver;

    public RatesDataHelper(String authority, ContentResolver contentResolver) {
        this.authority = authority;
        this.contentResolver = contentResolver;
    }

    public Observable<Map<String, Double>> getRatesAtTime(int secs) {
        BehaviorSubject<Map<String, Double>> getRatesForTimeSubject = BehaviorSubject.create();
        Map<String, Double> ratesMap = new HashMap<>();
        String querySelection = SQL.RATES_TABLE_SECOND_COLUMN_NAME + " = \'" + secs + "\'";
        Cursor cursor = contentResolver.query(MyContentProvider.getRatesUri(authority), null, querySelection, null, null);
        if (cursor == null) {
            throw new IllegalArgumentException("No Cursor created for table " + SQL.RATES_TABLE_NAME);
        }
        cursor.moveToFirst();
        String ratesJson = cursor.getString(cursor.getColumnIndex(SQL.RATES_TABLE_RATES_COLUMN_NAME));
        try {
            JSONObject jsonObject = new JSONObject(ratesJson);
            Iterator<String> iter = jsonObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                ratesMap.put(key, Double.parseDouble(jsonObject.optString(key)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cursor.close();
        getRatesForTimeSubject.onNext(ratesMap);
        return getRatesForTimeSubject;
    }

    public Observable<Map<String, Double>> getRatesBeforeTime(int secs) {
        BehaviorSubject<Map<String, Double>> getRatesForTimeSubject = BehaviorSubject.create();
        Map<String, Double> ratesMap = new HashMap<>();
        String querySelection = SQL.RATES_TABLE_SECOND_COLUMN_NAME + " < \'" + secs + "\'";
        Cursor cursor = contentResolver.query(MyContentProvider.getRatesUri(authority), null, querySelection, null, null);
        if (cursor == null) {
            throw new IllegalArgumentException("No Cursor created for table " + SQL.RATES_TABLE_NAME);
        }
        while (cursor.moveToNext()) {
            String ratesJson = cursor.getString(cursor.getColumnIndex(SQL.RATES_TABLE_RATES_COLUMN_NAME));
            Log.i("XXX", ratesJson);
//                try {
//                    JSONObject jsonObject = new JSONObject(ratesJson);
//                    Iterator<String> iter = jsonObject.keys();
//                    while (iter.hasNext()) {
//                        String key = iter.next();
//                        ratesMap.put(key, Double.parseDouble(jsonObject.optString(key)));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
        }
        getRatesForTimeSubject.onNext(ratesMap);
//        Log.i("XXX", "Rates Map " + ratesMap);
        return getRatesForTimeSubject;

    }

}
