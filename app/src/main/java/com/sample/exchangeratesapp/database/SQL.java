package com.sample.exchangeratesapp.database;

/**
 * Created by melvin on 24/10/17.
 */

public class SQL {

    public static final String TABLE_PRIMARY_KEY = "_id";
    public static final String ID_COLUMN_NAME = "id";

    public static final String CURRENCIES_TABLE_NAME = "currencies";
    public static final String CURRENCIES_TABLE_CURRENCY_CODE_COLUMN_NAME = "code";
    public static final String RATES_TABLE_NAME = "rates";
    public static final String RATES_TABLE_SECOND_COLUMN_NAME = "second";
    public static final String RATES_TABLE_RATES_COLUMN_NAME = "rates";

    public static final String [] ALL_TABLES = new String[]{
            CURRENCIES_TABLE_NAME,
            CURRENCIES_TABLE_CURRENCY_CODE_COLUMN_NAME
    };
}
