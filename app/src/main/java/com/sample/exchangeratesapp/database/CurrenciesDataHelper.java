package com.sample.exchangeratesapp.database;

import android.content.ContentResolver;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.BehaviorSubject;

/**
 * Created by melvin on 5/11/17.
 */

public class CurrenciesDataHelper {

    private String authority;
    private ContentResolver contentResolver;
    private BehaviorSubject<List<String>> currenciesListSubject;

    public CurrenciesDataHelper(String authority, ContentResolver contentResolver){
        this.authority = authority;
        this.contentResolver = contentResolver;
        currenciesListSubject = BehaviorSubject.create();
        makeQueryToCurrenciesTable();
    }

    private void makeQueryToCurrenciesTable(){
        List<String> currencies = new ArrayList<>();
        Cursor cursor = contentResolver.query(MyContentProvider.getCurrenciesUri(authority),
                null, null, null, null);
        if(cursor == null){
            throw new IllegalArgumentException("No Cursor created for Table " + SQL.CURRENCIES_TABLE_NAME);
        }
        while(cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex(SQL.ID_COLUMN_NAME));
            String currency = cursor.getString(cursor.getColumnIndex(SQL.CURRENCIES_TABLE_CURRENCY_CODE_COLUMN_NAME));
            currencies.add(currency);
        }
        currenciesListSubject.onNext(currencies);
        cursor.close();

    }

    public Observable<List<String>> getCurrenciesObservable(){
        return currenciesListSubject;
    }

}
