package com.sample.exchangeratesapp;

import android.view.View;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by melvin on 25/10/17.
 */

public class ObservableOps {

    public static Observable<View> clicks(View view) {
        final PublishSubject<View> subj = PublishSubject.create();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subj.onNext(v);

            }
        });
        return subj;
    }

}
